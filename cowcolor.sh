#!/bin/bash
# +-----------+
# |COLOR CODES|
# +-----------+
Black="0;30m";
Dark_Gray="1;30m";
Blue="0;34m";
Light_Blue="1;34m";
Green="0;32m";
Light_Green="1;32m";
Cyan="0;36m";
Light_Cyan="1;36m";
Red="0;31m";
Light_Red="1;31m";
Purple="0;35m";
Light_Purple="1;35m";
Brown="0;33m";
Yellow="1;33m";
Light_Gray="0;37m";
White="1;37m";

# +----------------------+
# |RANDOM COLOR GENERATOR|
# +----------------------+
rand1=$( echo {0..1} | sed s/" "/"\n"/g | shuf | head -n 1);
rand2=$( echo {30..37}m | sed s/" "/"\n"/g | shuf | head -n 1 );
RandomColor="$rand1;$rand2";

# +------------------------+
# |VARIABLES YOU CAN CHANGE|
# +------------------------+
color=$RandomColor
width="40";
fortunecommand=$(fortune);

# +-----------+
# |COW PRINTER|
# +-----------+
function DrawBox () {
box=""
for (( i=0; i<=$1+2; i++))
do
	box="-$box";
done;

echo -e $box;

while IFS= read -r line
do	
	whitespace="";
	characters=$( echo $line | wc -c );
	for (( i=$characters; i<$1+1; i++ ))
		do
			whitespace=" $whitespace";
		done;
		echo -e "| \033[$3$line\033[0m$whitespace|";
done <<< "$2";
echo -e $box;
}

cowsays=$( echo $fortunecommand | fold -w $width -s | sed 's/ *$//'  );
DrawBox "$width" "$cowsays" "$color";

# +------------------+
# |ADD YOUR COW BELOw|
# +------------------+

#echo -e "        \   ^__^"
#echo -e "         \  (oo)\_______"
#echo -e "            (__)\       )\\/\\ "
#echo -e "                ||----w |"
#echo -e "                ||     ||"

echo -e "             \            ,        , "
echo -e "               \         /(        )\` "
echo -e "                 \       \ \___   / | "
echo -e "                        /- _  \`-/  ' "
echo -e "                       (/\/ \ \   /\ "
echo -e "                       / /   | \`    \ "
echo -e "                       O O   ) /    | "
echo -e "                       \`-^--'\`<     ' "
echo -e "     .--.             (_.)  _  )   / "
echo -e "    |o_o |             \`.___/\`    / "
echo -e "    |:_/ |              \`-----' / "
echo -e "   //<- \ \----.     __ / __   \ "
echo -e "  (|  <- | )---|====O)))==) \) /==== "
echo -e " /'\ <- _/\`\---'    \`--' \`.__,' \ "
echo -e " \___)=(___/            |        | "
echo -e "                         \       / "
echo -e "                    ______( (_  / \______ "
echo -e "                  \,'  \,-----'   |        \ "
echo -e "                  \`--\{__________)        \/ "
